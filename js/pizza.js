var choosenpizza;
var choosengetraenk;
var choosenzusatz;
var preis1;
var preis2;
var positioner = 0;

$( document ).ready(function () {

	$(document).on('click','#modalagree',function()
	{
    $.ajax({
        url: index.php,
        type:'POST',
        data:
        {
            email: email_address,
            message: message
        },
        success: function(msg)
        {
            alert('Email Sent');
        }               
    });
	});


	$(".pizzabtn").click(function () {
		choosenpizza = $(this).attr("value");
		var preisstring = $(this).attr("preis");
		preis1 = parseFloat(preisstring);
		if(positioner == 1) {
		$("#abschlusstab").click();
		} else {		
		$("#extratab").click();
		}

	});

	$("#extratab").click(function () {
		
	});

	$(".zusatzbtn").click(function () {
		choosenzusatz = $(this).attr("value");
		var preisstring = $(this).attr("preis");
		preis2 = parseFloat(preisstring);
		if(positioner == 1) {
		$("#abschlusstab").click();
		} else {		
		$("#getraenktab").click();
		}
	
	});

	$("#getraenktab").click(function () {
		
	});

	$(".getraenkebtn").click(function () {
		choosengetraenk = $(this).attr("value");
		$("#abschlusstab").click();
	});

	$("#abschlusstab").click(function () {
		if(choosenpizza != null && choosenzusatz != null && choosengetraenk != null){
		positioner = 1;
		}
		$("#desc").html("");
		$("#preisresult").html("");
		if(preis1 == null){
			preis1 = 0;
		}
		if(preis2 == null){
			preis2 = 0;
		}
		var preis = preis1 + preis2;
		var preisstring = preis.toString();
		if(choosenpizza != null){var pizzasetornot = choosenpizza} else {var pizzasetornot = "__________"};
		if(choosenzusatz != null){var zusatzsetornot = choosenzusatz} else {var zusatzsetornot = "__________"};
		if(choosengetraenk != null){var getraenksetornot = choosengetraenk} else {var getraenksetornot = "__________"};
		if (preisstring != "NaN"){var preissetornot = preis.toFixed(2);}else{var preissetornot = "_____"};
		var string = ("Du bestellst dir eine Pizza "+ pizzasetornot +" mit "+ zusatzsetornot +" und als Getr&auml;nk ein "+ getraenksetornot +" :) ");
		var cost = ("CHF "+ preissetornot);
		$("#desc").append(string);
		$("#preisresult").append(cost);
		$("#phpform").append('<input type="text" style="display: none" class="hiddenphpfield" name="fpizza" value="'+ pizzasetornot +'">');
		$("#phpform").append('<input type="text" style="display: none" class="hiddenphpfield" name="fgetraenk" value="'+ zusatzsetornot +'">');
		$("#phpform").append('<input type="text" style="display: none" class="hiddenphpfield" name="fzusatz" value="'+ getraenksetornot +'">');
		$("#phpform").append('<input id="submitbtn" style="display: none" href="#" type="submit">');
	});


	$("#decline").click(function () {
		Materialize.toast('Bestellung abgebrochen!', 3000);
		setTimeout(back, 1500);
	});
		$("#accept").click(function () {
		Materialize.toast('Bestellung erfasst!', 1000);
		setTimeout(backacpt, 1000);
	});


});

function back() {
	$("#desc").html("");
	$("#preisresult").html("");
	var form = document.getElementById("phpform");
	form.reset();
	$("#pizzatab").click();
}

function backacpt() {
	$("#submitbtn").click();
	var form = document.getElementById("phpform");
	form.reset();
	$("#desc").html("");
	$("#preisresult").html("");
	$("#pizza").removeClass("hide");
	$("#confirm").addClass("hide");
}

function unsetsession () {
	window.location = "pages/unset.php";
}
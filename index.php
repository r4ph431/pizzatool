<?php
// Start the session
session_start();
$kartenleser = false;
//Use this if you want no PHP errors in your Page!
/*
error_reporting(0);
ini_set('display_errors', 0);
*/
?>
<html>
    <head>
      <!--materialize.min.css EDITED!-->
      <link rel="stylesheet" href="css/materialize.min.css"> 
      <link rel="stylesheet" href="css/style.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    </head>

    <body>
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script type="text/javascript" src="js/pizza.js"></script>
<?php
//if (!isset($_SESSION['username'])){
if(isset($_POST['username']) && !empty($_POST['username'])) {
$_SESSION['username'] = $_POST["username"];
}
if (!isset($_SESSION['username'])){

  echo '<script>
$(document).ready(function () {
$("#modal1").openModal();
});
</script>
<div id="modal1" class="modal">
<form action="" method="post">
    <div class="modal-content">
      <h5>Bitte w&auml;hlen</h5>
      <div class="input-field col s6">
          <input placeholder="Benutzername" name="username" type="text" class="validate">
        </div>
        <script>
        $(document).ready(function() {
        $("select").material_select();
        });
        </script>
        
    </div>
    <div class="modal-footer">
      <button href="#!" type="submit" class=" modal-action waves-effect waves-green btn-flat">Senden</button>
    </div>
    </form>
  </div>';

}
if(isset($_POST['username']) && !empty($_POST['username'])) {
$_SESSION['username'] = $_POST["username"];
}
?>



<div class="navbar-fixed">
<nav class="#2979ff blue accent-3">
    <div class="nav-wrapper">
      <a href="index.php" class="brand-logo center">Beta 0.1</a>
      <ul id="nav-mobile" class="right">
        <li><a href="pages/Kontakt.html">Info</a></li>
      </ul>
      <ul id="nav-mobile" class="left">
        <li><a href="pages/bestellung.php">Bestellung</a></li>
      </ul>
    </div>
  </nav>
</div>
<div class="row">
    <div class="col s12">
      <ul class="tabs">
        <li class="tab col s3"><a id="pizzatab" class="tabclass" href="#test1">Pizza</a></li>
        <li class="tab col s3"><a id="extratab" class="tabclass" href="#test2">Extra</a></li>
        <li class="tab col s3"><a id="getraenktab" class="tabclass" href="#test3">Getr&auml;nk</a></li>
        <li class="tab col s3"><a id="abschlusstab" class="tabclass" href="#test4">End</a></li>
      </ul>
    </div>
    <div id="test1" class="col s12"><ul class="collection" id="pizza">
    <li class="collection-item avatar">
      <img src="img/01.png" alt="" class="circle materialboxed">
      <span class="title">Margherita</span>
      <p class="pizzainfo">Tomatensauce, Mozzarella, Oregano <br>
         11.50 Fr
      </p>
      <a href="#!" class="secondary-content pizzabtn" preis="11.50" value="Margherita"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/02.png" alt="" class="circle materialboxed">
      <span class="title">Funghi</span>
      <p class="pizzainfo">Tomatensauce, Mozzarella, Champignons, Oregano <br>
         13.50 Fr
      </p>
      <a href="#!" class="secondary-content pizzabtn" preis="13.50" value="Funghi"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/03.png" alt="" class="circle materialboxed">
      <span class="title">Cipolla</span>
      <p class="pizzainfo">Tomatensauce, Mozzarella, Zwiebeln, Oregano <br>
         13.50 Fr
      </p>
      <a href="#!" class="secondary-content pizzabtn" preis="13.50" value="Cipolla"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/04.png" alt="" class="circle materialboxed">
      <span class="title">Thon</span>
      <p class="pizzainfo">Tomatensauce, Mozzarella, Thon, Oregano <br>
         13.50 Fr
      </p>
      <a href="#!" class="secondary-content pizzabtn" preis="13.50" value="Thon"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/08.png" alt="" class="circle materialboxed">
      <span class="title">Prosciutto</span>
      <p class="pizzainfo">Tomatensauce, Mozzarella, Schinken, Oregano <br>
         13.50 Fr
      </p>
      <a href="#!" class="secondary-content pizzabtn" preis="13.50" value="Prosciutto"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/09.png" alt="" class="circle materialboxed">
      <span class="title">Ostermundiger</span>
      <p class="pizzainfo">Tomatensauce, Mozzarella, Schinken, Zwiebeln, Oregano <br>
         13.50 Fr
      </p>
      <a href="#!" class="secondary-content pizzabtn" preis="13.50" value="Ostermundiger"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/10.png" alt="" class="circle materialboxed">
      <span class="title">Salami</span>
      <p class="pizzainfo">Tomatensauce, Mozzarella, Salami, Oregano <br>
         13.50 Fr
      </p>
      <a href="#!" class="secondary-content pizzabtn" preis="13.50" value="Salami"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/11.png" alt="" class="circle materialboxed">
      <span class="title">Hawaii</span>
      <p class="pizzainfo">Tomatensauce, Mozzarella, Schinken, Ananas, Oregano<br>
         13.50 Fr
      </p>
      <a href="#!" class="secondary-content pizzabtn" preis="13.50" value="Hawaii"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/16.png" alt="" class="circle materialboxed">
      <span class="title">Prosciutto &ndash; Funghi</span>
      <p class="pizzainfo">Tomatensauce, Mozzarella, Champignons, Schinken, Oregano <br>
         13.50 Fr
      </p>
      <a href="#!" class="secondary-content pizzabtn" preis="13.50" value="Prosciutto Funghi"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/17.png" alt="" class="circle materialboxed">
      <span class="title">K&ouml;nizer</span>
      <p class="pizzainfo">Tomatensauce, Mozzarella, Thon, Zwiebeln, Oliven, Oregano<br>
         13.50 Fr
      </p>
      <a href="#!" class="secondary-content pizzabtn" preis="13.50" value="K&ouml;nizer"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/26.png" alt="" class="circle materialboxed">
      <span class="title">Capricciosa</span>
      <p class="pizzainfo">Tomatensauce, Mozzarella, Champignons, Schinken, Ei, Oregano <br>
         13.50 Fr
      </p>
      <a href="#!" class="secondary-content pizzabtn" preis="13.50" value="Capricciosa"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/04.png" alt="" class="circle materialboxed">
      <span class="title">Kebab &ndash; Pizza</span>
      <p class="pizzainfo">Tomatensauce, Mozzarella, Kebabfleisch, Zwiebeln, Chili, Oregano <br>
         14.50 Fr
      </p>
      <a href="#!" class="secondary-content pizzabtn" preis="14.50" value="Kebab Pizza"><i class="material-icons">send</i></a>
    </li>
  </ul></div>
    <div id="test2" class="col s12"><ul class="collection" id="Zusatz">
    <li class="collection-item avatar">
      <img src="img/Cocktail.png" alt="" class="circle">
      <span class="title">Cocktail Sauce</span>
      <p> + 0 Fr
      </p>
      <a href="#!" class="secondary-content zusatzbtn" preis="0" value="Cocktail Sauce"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/MOZZARELLA.gif" alt="" class="circle">
      <span class="title">Extra K&auml;se</span>
      <p> + 2 Fr
      </p>
      <a href="#!" class="secondary-content zusatzbtn" preis="2" value="Extra K&auml;se"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <span class="title">Keine Extra</span>
      <a href="#!" class="secondary-content zusatzbtn" preis="0" value="keinem Extra"><i class="material-icons">send</i></a>
    </li>

  </ul></div>
    <div id="test3" class="col s12"><ul class="collection" id="getraenk">
    <li class="collection-item avatar">
      <img src="img/Fanta.png" alt="" class="circle getraenk">
      <span class="title">Fanta</span>
      </p>
      <a href="#!" class="secondary-content getraenkebtn" value="Fanta"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/rivella.png" alt="" class="circle getraenk">
      <span class="title">Rivella</span>
      </p>
      <a href="#!" class="secondary-content getraenkebtn" value="Rivella"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/icetealemon.png" alt="" class="circle getraenk">
      <span class="title">Ice Tea Lemon</span>
      </p>
      <a href="#!" class="secondary-content getraenkebtn" value="Ice Tea Lemon"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/iceteapeach.png" alt="" class="circle getraenk">
      <span class="title">Ice Tea Peach</span>
      </p>
      <a href="#!" class="secondary-content getraenkebtn" value="Ice Tea Peach"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/cocacola.png" alt="" class="circle getraenk">
      <span class="title">Cola</span>
      </p>
      <a href="#!" class="secondary-content getraenkebtn" value="Cola"><i class="material-icons">send</i></a>
    </li>
    <li class="collection-item avatar">
      <img src="img/cocacolazero.png" alt="" class="circle getraenk">
      <span class="title">Cola-Zero</span>
      </p>
      <a href="#!" class="secondary-content getraenkebtn" value="Cola-Zero"><i class="material-icons">send</i></a>
    </li>
  </ul></div>
    <div id="test4" class="col s12"><div class="row" id="confirm">
        <div class="col s12 m6">
          <div class="card #1976d2 blue darken-2">
            <div class="card-content white-text">
              <span class="card-title" id="preisresult"></span>
              <p id="desc" class="description"></p>
            </div>
            <div class="card-action">
              <div class="row">
                
              <form method="post" id="phpform" action="<?php echo $_SERVER['PHP_SELF'];?>">
                <br \>
                <div class="switch" style="color: white">
                  Kartenleseger&auml;t: &nbsp;&nbsp;&nbsp;
                <label style="color: white !important">
                  Nein
                <input type="checkbox" name="kartenleser">
                <span class="lever"></span>
                  Ja
                </label>
                </div>
              </form>
              </div>
              <a id="accept" style="color: #b3e5fc !important" href="#">Abschliessen</a>
              <a id="decline" style="color: #b3e5fc !important" href="#">Abbrechen</a>
            </div>
          </div>
        </div>
      </div></div>
  </div>





  
  
<?php
  $kartenleser = false;
  include 'pages/dbconnect.php';
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // collect value of input field
    $ppizza = $_POST['fpizza'];
    $pgetraenk = $_POST['fgetraenk'];
    $pzusatz = $_POST['fzusatz'];
    $pusername = $_SESSION['username'];
    $username = htmlentities($pusername);
    $pizza = htmlentities($ppizza);
    $zusatz = htmlentities($pzusatz);
    $kartenleser = $_POST['kartenleser'];
    if ($kartenleser == "on") {
      $kartenleser = true;
    }
    if(isset($_REQUEST['fpizza'])) {
      $sql = "INSERT INTO orders (Pizza, getraenk, Zusatz, username, kartenleser)
              VALUES ('$pizza', '$pgetraenk', '$zusatz', '$username', '$kartenleser')";

      if ($conn->query($sql) === TRUE) {
          
      } else {
          echo "Error: " . $sql . "<br>" . $conn->error;
      }
      $_POST['fpizza'] = "";
      unset($_POST);
      $_POST = array();
      $conn->close();
    }
  }
?>
</body>
</html>

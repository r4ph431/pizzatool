<?php
// Start the session
session_start();
/*
error_reporting(0);
ini_set('display_errors', 0);
*/
?>
<html>
    <head>
      <!--Import Google Icon Font-->
      <link rel="stylesheet" href="../css/materialize.min.css">
      <link href="../css/google.css" rel="stylesheet">
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>
<div class="navbar-fixed">
<nav class="#2979ff blue accent-3">
    <div class="navbar-fixed">
      <a href="../index.php" class="brand-logo center">Beta 0.1</a>
      <ul id="nav-mobile" class="right">
        <li><a href="Kontakt.html">Info</a></li>
      </ul>
      <ul id="nav-mobile" class="left">
        <li><a href="bestellung.php">Bestellung</a></li>
      </ul>
    </div>
  </nav>
</div>
<h4>Pizzas</h4>
        <table class="">
        <thead>
            <tr>
                <th data-field="Pizza">Pizza</th>
                <th data-field="Zusatz">Zusatz</th>
                <th data-field="Besteller">Besteller</th>
            </tr>
        </thead>
        <tbody>
            <?php
            include '../pages/dbconnect.php';
            $kartenleserboolean = 0;
            $sql = "SELECT Pizza, Zusatz, username, kartenleser FROM orders WHERE DATE(Datum) = DATE(NOW()) ORDER BY Pizza ASC;";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
              if($row['kartenleser'] == 1){
                $kartenleserboolean =  1;
              };

            ?>
                <tr>
                    <td class="pizzaresult"><?php echo $row['Pizza'];?></td>
                    <td><?php echo $row['Zusatz'];?></td>
                    <td><?php echo $row['username'];?></td>

                </tr>

            <?php



            }

          }

            ?>
            </tbody>
            </table>

            <h4> Getr&auml;nke </h4>
            <table class="">
        <thead>
            <tr>
                <th data-field="Getr&auml;nk">Getr&auml;nk</th>
                <th data-field="Besteller">Besteller</th>
            </tr>
        </thead>
        <tbody>
            <?php
            include '../pages/dbconnect.php';
            $sql = "SELECT getraenk, username FROM orders WHERE DATE(Datum) = DATE(NOW()) ORDER BY getraenk ASC;";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
            ?>
                <tr>
                    <td><?php echo $row['getraenk'];?></td>
                    <td><?php echo $row['username'];?></td>

                </tr>

            <?php



            }

          }

            ?>
            </tbody>
            </table>
            <?php
            if ($kartenleserboolean == 1) {
              echo "<h4 style='color:red'>Kartenleser ben&ouml;tigt</h4>";
            };
            
            ?>


</body>
</html>

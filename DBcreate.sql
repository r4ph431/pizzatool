-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 01. Okt 2015 um 12:33
-- Server-Version: 5.6.26
-- PHP-Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `pizzatool`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `orders`
--
CREATE DATABASE IF NOT EXISTS pizzatool;
USE pizzatool;

CREATE TABLE IF NOT EXISTS `orders` (
  `Id` int(11) NOT NULL,
  `Pizza` varchar(50) NOT NULL,
  `getraenk` varchar(50) NOT NULL,
  `Zusatz` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `kartenleser` tinyint(1) NOT NULL,
  `Datum` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=latin1;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--
